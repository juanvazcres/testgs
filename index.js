const { MongoClient } = require('mongodb');

async function main() {
    const uri = 'mongodb+srv://juanvazcres:0945@cluster0.6osa1.mongodb.net/test';
    const client = new MongoClient(uri);

    try {

        await client.connect();
        //1.-Devolver el numero de registros con la categoría Abarrotes y Cocina, así mismo cuántos pertenecen a la categoría Aluminio y Herrería.
        await countAbarrotesYCocina(client);
        await countAluminioYHerreria(client);
        //2.-Devolver cuantas tiendas tienen más de una sub categoría.
        await countMoreThanOneCategory(client);
        //3.-Tiendas creadas después del 04/04/2022.
        await countAfterDateStores(client);

    } catch (e) {
        console.error(e);
    } finally {
        await client.close();
    }
}

async function countAbarrotesYCocina(client) {
    collection = await client.db('Test').collection('Tiendas');
    const cursor = collection.find({ 'categoria.nombre': 'Abarrotes y Cocina' });
    const count = await cursor.count();
    console.log("cantidad de Abarrotes y Cocina", count);
}

async function countAluminioYHerreria(client) {
    collection = await client.db('Test').collection('Tiendas');
    const cursor = collection.find({ 'categoria.nombre': 'Aluminio y Herrería' });
    const count = await cursor.count();
    console.log("cantidad de Aluminio y Herrería", count);
}

async function countMoreThanOneCategory(client) {
    collection = await client.db('Test').collection('Tiendas');
    const cursor = collection.find({ 'categoria.subcategorias.1': { $exists: true } });
    const count = await cursor.count();
    console.log("cantidad de más de 1 sub categorías", count);
}

async function countAfterDateStores(client) {
    collection = await client.db('Test').collection('Tiendas');
    const cursor = collection.find({ 'categoria.fechaHoraAlta': {} });//TODO
    const count = await cursor.count();
    console.log("cantidad de Tiendas después de 04/04/2022", count);
}

main().catch(console.error);